package gosh
// Copyright (C) 2022 Naomi Steele - see LICENSE

import (
	"os/exec"
)

func buildCommand(command string) *exec.Cmd {
	return exec.Command("/bin/sh", "-c", command)
}
