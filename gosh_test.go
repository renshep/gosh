package gosh
// Copyright (C) 2022 Naomi Steele - see LICENSE

import (
	"fmt"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	r := m.Run()
	os.Exit(r)
}

func testSh(t *testing.T, cmd string, expectedReturn int, expectedError bool) {
	fmt.Println("Testing:", cmd)
	r, err := Sh(cmd)
	fmt.Printf("    Returncode: %v, Error: %v", r, err)
	if r != expectedReturn {
		t.Fatal("    Expected ErrorCode:", expectedReturn, "Got ErrorCode:", r)
	}
	if expectedError {
		if err == nil {
			t.Fatal("Expected Error, but err is nil")
		}
	} else {
		if err != nil {
			t.Fatal("Didn't expect error, but got:", err)
		}
	}
	fmt.Println(", OK!")
}

func TestSh(t *testing.T) {
	TestSh_windows(t)
	TestSh_unix(t)
}
