package gosh
// Copyright (C) 2022 Naomi Steele - see LICENSE

import (
	"testing"
)

func TestSh_windows(t *testing.T) {
	// do nothing
}

func TestSh_unix(t *testing.T) {
	testSh(t, "/bin/true", 0, false)
	testSh(t, "/bin/sleep 0.1", 0, false)
	testSh(t, "echo hello world", 0, false)
	testSh(t, "/bin/false", 1, false)
	testSh(t, "/bin/4a93a670-3a70-4c0e-9f10-150597c40993", 127, false)
	testSh(t, "( ( false ) ; exit 0 )", 0, false)
}
