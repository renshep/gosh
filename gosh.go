package gosh
// Copyright (C) 2022 Naomi Steele - see LICENSE

import (
	"os"
	"os/exec"
)

func Sh(command string) (exitcode int, err interface{}) {
	cmd := buildCommand(command)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd_err := cmd.Run()
	if cmd_err != nil {
		if ce, ce_ok := cmd_err.(*exec.ExitError); ce_ok {
			return ce.ProcessState.ExitCode(), nil
		} else {
			return -1, cmd_err
		}
	}
	return 0, nil
}

func Shx(command string) (exitcode int, stdout, stderr string, err interface{}) {
	return 0, "", "", nil
	// TODO
}
